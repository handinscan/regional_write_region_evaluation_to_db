package com.handinscan.regional;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class WriteRegionEvaluationToDb {
	private static final Logger logger = LoggerFactory.getLogger(WriteRegionEvaluationToDb.class);
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, TimeoutException {
		ConfigurableApplicationContext context = SpringApplication.run(WriteRegionEvaluationToDb.class, args);
		
		ApplicationController applicationController = context.getBean(ApplicationController.class);
		applicationController.start();
		logger.debug("Created ApplicationController" + applicationController);
	}
}

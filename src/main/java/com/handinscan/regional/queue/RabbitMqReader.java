package com.handinscan.regional.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

@Component
public class RabbitMqReader implements InitializingBean, DisposableBean {
	private final Logger logger = LoggerFactory.getLogger(RabbitMqReader.class);
	
	private java.util.function.Consumer<String> callback;
	
	@Value("${handinscan.regional.queuename}")
	private String queueName;
	
	@Value("${handinscan.regional.username}")
	private String userName;
	
	@Value("${handinscan.regional.password}")
	private String password;
	
	@Value("${handinscan.regional.virtualhostname}")
	private String virtualHostName;
	
	@Value("${handinscan.regional.host}")
	private String host;
	
	@Value("${handinscan.regional.port}")
	private int port;
	
	@Value("${handinscan.regional.exchangename}")
	private String exchangeName;
	
	@Value("${handinscan.regional.routingkey}")
	private String routingKey;

	private Connection connection;

	private Channel channel;
	
	@Override
	public void afterPropertiesSet() throws IOException, TimeoutException {
		connection = createConnection();
	    channel = connection.createChannel();

	    channel.queueDeclare(queueName, false, false, false, null);
	    logger.debug(".... created connection, channel etc in RabbitMqReader: " + this.toString());
	}
	
	private Connection createConnection() throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setUsername(userName);
		factory.setPassword(password);
		factory.setVirtualHost(virtualHostName);
		factory.setHost(host);
		factory.setPort(port);
		Connection connection = factory.newConnection();
		return connection;
	}
	
	public void start() throws IOException {
	    Consumer consumer = new DefaultConsumer(channel) {
		    @Override
		    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
		    	logger.debug("Incoming message: '" + body + "'");
		    	if (callback != null) {
		    		String message = new String(body, "UTF-8");
		    		callback.accept(message);
		    	}
			}
		};
	    channel.basicConsume(queueName, true, consumer);
	}
	
	public void setCallback(java.util.function.Consumer<String> callback) {
		this.callback = callback;
	}

	@Override
	public void destroy() throws Exception {
		logger.debug(".... closing connections");
		channel.close();
		connection.close();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RabbitMqReader [logger=").append(logger)
				.append(", queueName=").append(queueName).append(", userName=").append(userName).append(", password=")
				.append(password).append(", virtualHostName=").append(virtualHostName).append(", host=").append(host)
				.append(", port=").append(port).append(", exchangeName=").append(exchangeName).append(", routingKey=")
				.append(routingKey).append("]");
		return builder.toString();
	}
}

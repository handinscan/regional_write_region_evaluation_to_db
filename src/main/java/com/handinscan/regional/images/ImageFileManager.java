package com.handinscan.regional.images;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.handinscan.regional.data.dto.Region;

@Component
public class ImageFileManager implements InitializingBean {
	private final Logger logger = LoggerFactory.getLogger(ImageFileManager.class);
	
	@Value("${handinscan.regional.tempimagedirectory}")
	private String imageFolderPath;
	
	private File imageFolder;
	
	@Override
	public void afterPropertiesSet() {
		imageFolder = new File(imageFolderPath);
	}

	public void deleteImageFile(Region region) throws IOException {
		String imageFileNameShouldBe = region.getMeasurementId() + "_" + region.getName().toLowerCase() + ".png";
		logger.info(".... From region: " + region.toString() + ", image file name should be: " + imageFileNameShouldBe);

		File imageObjectFromRegion = new File(imageFolderPath +"/" + imageFileNameShouldBe);
		boolean doesImageFileExist = imageObjectFromRegion.exists();
		logger.info(".... imageObjectFromRegion exists: " + doesImageFileExist);
		if (doesImageFileExist) {
			boolean deleted = imageObjectFromRegion.delete();
			logFileDeletion(imageObjectFromRegion, deleted);
		} else {
			logger.warn(".... image file " + imageFileNameShouldBe + " does not exist in folder: " + imageFolder.getCanonicalPath());
		}
	}

	public void deleteImageFile(int measurementId, String name) throws IOException {
		String imageFileNameShouldBe = measurementId + "_" + name.toLowerCase() + ".png";
		logger.info(".... From measurementId, name: " + measurementId + ", " + name + ", image file name should be: " + imageFileNameShouldBe);
		
		File imageObjectFromRegion = new File(imageFolderPath +"/" + imageFileNameShouldBe);
		boolean doesImageFileExist = imageObjectFromRegion.exists();
		logger.info(".... imageObjectFromRegion exists: " + doesImageFileExist);
		if (doesImageFileExist) {
			boolean deleted = imageObjectFromRegion.delete();
			logFileDeletion(imageObjectFromRegion, deleted);
		} else {
			logger.warn(".... image file " + imageFileNameShouldBe + " does not exist in folder: " + imageFolder.getCanonicalPath());
		}
	}
	
	private void logFileDeletion(File imageFile, boolean fileDeleted) throws IOException {
		if (fileDeleted) {
			logger.debug(".... Successfully deleted image file at path: '" + imageFile.getCanonicalPath() + "'");
		} else {
			logger.error(".... Could not delete image file at path: '" + imageFile.getCanonicalPath() + "'!!");
		}
	}
}

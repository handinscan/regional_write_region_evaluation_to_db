package com.handinscan.regional;

import java.io.IOException;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.handinscan.regional.data.DatabaseController;
import com.handinscan.regional.data.dto.RegionEvaluation;
import com.handinscan.regional.mapper.QueueToDbMessageMapper;
import com.handinscan.regional.queue.RabbitMqReader;

@Component
public class ApplicationController {
	private final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
	
	@Autowired
	RabbitMqReader mqReader;
	
	@Autowired
	QueueToDbMessageMapper mapper;
	
	@Autowired
	DatabaseController dbWriter;

	public void start() throws IOException {
		mqReader.setCallback(new Consumer<String>(){
			@Override
			public void accept(String message) {
				try {
					manageMessage(message);
				} catch (IOException e) {
					logger.error(".... Could not manage incoming message: " + message);
					logger.error(".... caused by: " + e.getMessage());
					e.printStackTrace();
				}
			}
		});
		mqReader.start();
	}
	
	private void manageMessage(String message) throws IOException {
		logger.info(".... incoming message: " + message);
		RegionEvaluation evaluation = mapper.map(message);
		dbWriter.handleEvaluation(evaluation);
	}
}

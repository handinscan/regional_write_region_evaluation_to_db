package com.handinscan.regional.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.handinscan.regional.data.dto.Region;
import com.handinscan.regional.data.dto.RegionEvaluation;
import com.handinscan.regional.images.ImageFileManager;

@Component
public class DatabaseController {
	private static final String SIDE_INFO_PART_SEPARATOR = "_";
	
	private final Logger logger = LoggerFactory.getLogger(DatabaseController.class);
	
	@Autowired
	private ImageFileManager imageManager;
	
	@Autowired
	private RegionRepository repository;
	
	public void handleEvaluation(RegionEvaluation evaluation) throws IOException {
	      List<Region> regions = createRegionsToBeInserted(evaluation);
	      insertRegionsIntoDb(regions);
	}
	
	private List<Region> createRegionsToBeInserted(RegionEvaluation evaluation) {
		List<Region> result = new ArrayList<>();
		
		for (short position = 1; position <= 10; position++) {
			boolean value = evaluation.getRegionEvaluation(position);
			Region region = createRegionWithEvaluationData(evaluation.getId(), position, value);
			result.add(region);
		}
		
		return result;
	}

	private Region createRegionWithEvaluationData(String idString, short position, boolean value) {
		Region region = new Region();
		int id = parseIdFromEvaluation(idString);
		region.setMeasurementId(id);
		
		String leftRightPalmDorsum = parseSideInfoFromEvaluation(idString);
		region.setName(leftRightPalmDorsum);
		
		region.setPosition(position);
		short shortValue = (short)(value ? 1 : 0);
		region.setValue(shortValue);
		
		logger.debug("........ created Region to be inserted: " + region);
		
		return region;
	}

	private int parseIdFromEvaluation(String evalIdString) {
		String[] evalIdStringParts = evalIdString.split(SIDE_INFO_PART_SEPARATOR);
		int id = Integer.parseInt(evalIdStringParts[0]);
		return id;
	}
	
	private String parseSideInfoFromEvaluation(String evalIdString) {
		String[] evalIdStringParts = evalIdString.split(SIDE_INFO_PART_SEPARATOR);
		String leftRight = evalIdStringParts[1];
		String palmDorsum = evalIdStringParts[2];
		String palmDorsumCleaned = removePngEnding(palmDorsum);
		String sideInfo = leftRight.toUpperCase() + SIDE_INFO_PART_SEPARATOR + palmDorsumCleaned.toUpperCase();
		return sideInfo;
	}

	private String removePngEnding(String palmDorsum) {
		if (!palmDorsum.endsWith(".png") && !palmDorsum.endsWith(".PNG")) {
			return palmDorsum;
		}
		int length = palmDorsum.length();
		String result = palmDorsum.substring(0, length - 4);
		return result;
	}

	@Transactional
	private void insertRegionsIntoDb(List<Region> regions) throws IOException {
		if (regions.size() == 0) {
			return;
		}
		Region firstRegion = regions.get(0);
		int measurementId = firstRegion.getMeasurementId();
		String leftRightPalmDorsum = firstRegion.getName();
		List<Region> regionDataOfMeasurementAndName = repository.findByMeasurementIdAndName(measurementId, leftRightPalmDorsum);
		logger.debug("........ all previous regions belong to measurementId: " + measurementId + " and name: " + leftRightPalmDorsum + " are going to be deleted");
		repository.deleteInBatch(regionDataOfMeasurementAndName);
		
		repository.save(regions);

		Region region = regions.get(0);
		imageManager.deleteImageFile(region.getMeasurementId(), region.getName());
	}
}

package com.handinscan.regional.data.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Region {
	@Id
	@GeneratedValue
	private int id;
	
	@Column(name = "measurementID")
	private int measurementId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "value")
	private short value;
	
	@Column(name = "position")
	private short position;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMeasurementId() {
		return measurementId;
	}

	public void setMeasurementId(int measurementId) {
		this.measurementId = measurementId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public short getPosition() {
		return position;
	}

	public void setPosition(short position) {
		this.position = position;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Region [id=").append(id).append(", measurementId=").append(measurementId).append(", name=")
				.append(name).append(", value=").append(value).append(", position=").append(position).append("]");
		return builder.toString();
	}

}

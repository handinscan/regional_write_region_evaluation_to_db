package com.handinscan.regional.data.dto;

import java.util.HashMap;
import java.util.Map;

public class RegionEvaluation {
	private String id;
	private Map<Short, Boolean> regionEvaluations = new HashMap<>();
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public boolean getRegionEvaluation(short position) {
		return regionEvaluations.get(position);
	}
	
	public void addRegionEvaluation(short position, boolean value) {
		regionEvaluations.put(position, value);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RegionEvaluation [id=").append(id).append(", regionEvaluations=").append(regionEvaluations)
				.append("]");
		return builder.toString();
	}
}

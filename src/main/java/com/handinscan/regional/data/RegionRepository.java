package com.handinscan.regional.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.handinscan.regional.data.dto.Region;

public interface RegionRepository extends JpaRepository<Region, Integer>{
	List<Region> findByMeasurementIdAndName(int measurementId, String name);
}

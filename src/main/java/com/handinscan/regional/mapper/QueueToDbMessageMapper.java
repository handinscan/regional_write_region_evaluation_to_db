package com.handinscan.regional.mapper;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.handinscan.regional.data.dto.RegionEvaluation;

@Component
public class QueueToDbMessageMapper {
	private static final String MESSAGE_PART_SEPARATOR = ";";
	private final Logger logger = LoggerFactory.getLogger(QueueToDbMessageMapper.class);;
	
	public RegionEvaluation map(String message) throws UnsupportedEncodingException {
	      RegionEvaluation evaluation = new RegionEvaluation();
	      fillInRegionEvaluation(evaluation, message);
	      return evaluation;
	}

	private void fillInRegionEvaluation(RegionEvaluation evaluation, String received) {
		String[] messageParts = received.split(MESSAGE_PART_SEPARATOR);

		evaluation.setId(messageParts[0]);
		
		for (short messagePartIndex = 1; messagePartIndex <= 10; messagePartIndex++) {
			String part = messageParts[messagePartIndex];
			evaluation.addRegionEvaluation(messagePartIndex, parseBooleanFromString(part));
		}
	}

	private boolean parseBooleanFromString(String part) {
		if (part.equals("0")) {
			return false;
		} else if (part.equals("1")) {
			return true;
		}
		String errorMessage = "Message part: '" + part + "' is invalid, expected '0' or '1' as evaluation result";
		logger.error(errorMessage);
		throw new IllegalArgumentException(errorMessage);
	}
}
